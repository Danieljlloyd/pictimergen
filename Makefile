all: pictimergen

pictimergen: pictimergen.c
	gcc $< -o $@

install:
	install -m0755 pictimergen /usr/local/bin
