/*
This is a script that generates the source code needed to generate the
correct timer frequency for a PIC microcontroller.
*/

#include <stdio.h>
#include <stdlib.h>

const char usage [] = "Usage: pictimergen fclk period_us timernum\n";

struct Prescaler {
    int scale;
    int code;
};

const struct Prescaler available_prescalers[9] = {
    {1, 8}, {2, 0}, {4, 1}, {8, 2}, {16, 3},
    {32, 4}, {64, 5}, {128, 6}, {256, 7}
};

int main(int argc, char *argv[])
{
	if (argc < 3) {
	    fputs(usage, stderr);
	    exit(-1);
	}

	int fclk = strtol(argv[1], NULL, 10);
	int period = strtol(argv[2], NULL, 10);
	int timernum = strtol(argv[3], NULL, 10);

	int reg_value;
	int option_reg = 0;
	struct Prescaler prescaler;

	for (int i=0; i<9; i++) {
		prescaler = available_prescalers[i];

		reg_value = 256 - fclk / 1000000 * period
			/ (prescaler.scale * 4);

		if (reg_value > 0 && reg_value < 256)
			break;
	}

	option_reg |= prescaler.code;

	printf("void interrupt timer%d_isr()\n", timernum);
	puts("{");
	puts("\tif (TMR0IF == 1) {");
	printf("\t\tTMR0 = %d + 1;\n", reg_value);
	puts("\t\tTMR0IF = 0;");
	puts("\t}");
	puts("}");
	puts("\n");
	printf("int timer%d_setup(void)\n", timernum);
	puts("{");
	printf("\tOPTION_REG = %d\n", option_reg);
	printf("\tTMR0 = %d;\n", reg_value);
	puts("\tT0SE = 1;");
	puts("\tT0CS = 0;");
	puts("}");
}
